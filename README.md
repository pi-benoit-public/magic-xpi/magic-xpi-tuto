# Magic xpi - tutos

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

## Notes

Les tutoriels sont enregistrés en markdown et écrits en français.

## Sommaire

* ['business process' : ajouter et renommer -> ./tutos/business-process-add-rename.md](./tutos/business-process-add-rename.md "'business process' : ajouter et renommer")

* ['flow' : Ajouter et renommer un flux et une description -> ./tutos/flow-add-rename.md](./tutos/flow-add-rename.md "'flow' : Ajouter et renommer un flux et une description")

* ['flow' : Ajouter des variables de flux -> ./tutos/flow-add-variable.md](./tutos/flow-add-variable.md "'Flow' : Ajouter des variables de flux")

* ['Trigger' - 'Directory Scanner' : Pour scanner un répertoire -> ./tutos/directory-scanner-trigger.md](./tutos/directory-scanner-trigger.md "'Trigger' - 'Directory Scanner' : Pour scanner un répertoire")

* ['Utilities' - 'Data Mapper' : Pour ne pas enregistrer certaines lignes comme les en-têtes -> ./tutos/data-mapper-skip-header.md](./tutos/data-mapper-skip-header.md "'Utilities' - 'Data Mapper' : Pour ne pas enregistrer certaines lignes comme les en-têtes")

* ['Utilities' - 'Data Mapper' : Condition selon un champ -> ./tutos/condition-by-field.md](./tutos/condition-by-field.md "'Utilities' - 'Data Mapper' : Condition selon un champ")

* [Fichier CSV vers BDD Salesforce -> ./tutos/file-csv-to-db-salesforce.md](./tutos/file-csv-to-db-salesforce.md "fichier CSV vers BDD Salesforce")

* [Exemples -> ./tutos/examples.md](./tutos/examples.md "Exemples")

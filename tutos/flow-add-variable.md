[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# 'flow' : Ajouter des variables de flux

1. Dans **Solution Explorer**, sous le flux choisi, double cliquer sur **Flow Variables**.
2. Ajouter vos variables de flux selon vos critères. Par convention, une variable de flux commence par `F.` suivi de son nom.

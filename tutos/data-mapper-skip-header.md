[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# 'Utilities' - 'Data Mapper' : Pour ne pas enregistrer certaines lignes comme les en-têtes

Si le fichier source contient des en-têtes, par exemple, voici un moyen pour éviter que cette ligne ne soit enregistrée.

## Méthode 1 : avec un compteur

1. Créer une variable de flux nommée par exemple : `F.Counter` de type **Numeric** et de **Length** : **12.0** (.0 correspond au nombre de chiffre après la virgule).

2. Dans **Data Mapper/Destination**, clic droit sur le 1er niveau (1er dossier).
Sélectionner **Expand all**.

3. Cliquer sur **Record** ou **Row** ..., selon.

4. Dans **Properties/General/Multi Updates**, cliquer sur l’icône **...** (3 petits points).

5. Une fenêtre **Multi Updates** apparaît. Cliquer sur **Add**.
    * Champ **Variable Name** : ajouter votre variable de flux : `F.Counter`.
    * Champ **Expression** : ajouter `F.Counter + 1`. Ceci permettra l’incrémentation du compteur à chaque itération.

Cliquer sur le point d’exclamation blanc sur fond rouge. Si tout est correct. Il disparaît.

Cliquer sur **OK**. La fenêtre disparaît.

6. Toujours positionner sur **Record** ou **Row** (selon), dans **Properties/General/Condition**, ajouter `F.Counter <> 1`. Ceci aura pour effet de sauter la 1ère ligne du schéma de données donc d’ignorer la ligne d’en-tête.

## Méthode 2 : correspondance d'une chaîne de caractères avec un nom de champ

1. Dans la **Destination**, clic droit sur le 1er niveau (1er dossier). Sélectionner **Expand all**.

2. Cliquer sur **Record** ou **Row** ..., selon.

3. Dans les **Properties/General/Condition** de **Record** ou **Row**, cliquer sur l’icône **...** (3 petits points). Une fenêtre **Destination/[nom_de_la_table]/row : Condition [Logical]** apparaît.

4. Sélectionner dans **Source Nodes** (menu déroulant avec comme icône **</>**), la variable correspondant à un champ d’en-tête commençant généralement par **Src.S1/Record/[nom_du_champ]**.
La condition a ajouté est : `Src.S1/Record/[nom_du_champ] <> [nom_du_champ]` où **<>** correspond à différent de.

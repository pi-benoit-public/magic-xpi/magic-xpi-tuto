[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# Exemples

## Transformer une date avec une heure

Format source : **YYYY-MM-DD HHhMM**

Format de destination : **DD-MM-YYYY HHhMM**

Type : Alpha

Code : `DStr ( DVal (Src.S1/Record/date, 'YYYY-MM-DD'), 'DD-MM-YYYY') & ' ' & TStr ( TVal (MID (Src.S1/Record/date, 12,5), 'HHhMM'), 'HH:MM')`


---

## Générer une date (maintenant) avec une heure fixe (6h00) (UTC+00:00)

Format de sortie : **YYYY-MM-DDT06:00:000Z**

Type : Alpha

Code : `DStr (Date (), 'YYYY-MM-DD') & 'T06:00:00.000Z'`


---

## Générer une date (maintenant) et une heure (maintenant)

Format de sortie : **YYYYMMDD-HHMMSS**

Type : Alpha

Code : `DStr (Date (), 'YYYYMMDD') & '-' & TStr (Time (), 'HHMMSS')`


---

## Récupérer le nom d'un fichier dans un chemin complet

F.FileName : chemin complet de l'emplacement d'un fichier (exemple : C:\Users\pi-benoit\Desktop\test.txt)

Format de sortie : **nomFichier.extension** (exemple : test.txt)

Type : Alpha

Code : `StrToken (  Trim (F.FileName), StrTokenCnt (Trim (F.FileName), '\'), '\')`

*Attention ! Utiliser des `\` pour Windows et des `/` pour Unix (Linux/Mac)*


---

## Récupérer le nom d'un fichier dans un chemin complet sans l'extension

C.FileNameDestination : chemin complet de l'emplacement d'un fichier (exemple : C:\Users\pi-benoit\Desktop\test.txt)

Format de sortie : **nomFichier** (exemple : test)

Type : Alpha

Code version (sur plusieurs lignes) :
```
DelSubStr (
 StrToken ( Trim (C.FileNameDestination), StrTokenCnt (Trim (C.FileNameDestination), '\'), '\'),
 Len (
  StrToken ( Trim (C.FileNameDestination), StrTokenCnt (Trim (C.FileNameDestination), '\'), '\')
 ) - 3,
 4
)
```

Code version (sur une ligne) : `DelSubStr (
 StrToken ( Trim (C.FileNameDestination), StrTokenCnt (Trim (C.FileNameDestination), '\'), '\'),
 Len (
  StrToken ( Trim (C.FileNameDestination), StrTokenCnt (Trim (C.FileNameDestination), '\'), '\')
 ) - 3,
 4
)`

*Attention ! Utiliser des `\` pour Windows et des `/` pour Unix (Linux/Mac)*


---

## Remplace un caractère ou une chaîne de caractères par une autre dans un data mapper.

Exemple :
`
RepStr (Src.S1/Record/id, '"', '')
`


---

## Ne traite pas certaines lignes avec deux champs/chaînes de caractères en condition dans un data mapper.

Exemple :
`
(Src.S1/Record/date <> 'date') AND (Src.S1/Record/nom <> 'nom')
`

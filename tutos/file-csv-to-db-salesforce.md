[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# Fichier CSV vers BDD Salesforce : Ajout de données en provenance d’un fichier CSV d'un répertoire local vers une base de données SalesForce

## 1. Ajouter un flux

[Ajouter un flux -> ./tutos/flow-add-rename.md](./tutos/flow-add-rename.md "Ajouter un flux")

---

## 2. Renommer le flux

[Renommer un flux -> ./tutos/flow-add-rename.md](./tutos/flow-add-rename.md "Renommer un flux")

---

## 3. Ajouter une description au flux
[Ajouter une description pour un flux -> ./tutos/flow-add-rename.md](./tutos/flow-add-rename.md "Ajouter une description pour un flux")

---

## 4. Créer des variables de flux

[Ajouter des variables de flux -> ./tutos/flow-add-variable.md](./tutos/flow-add-variable.md "Ajouter des variables de flux")

Créer des variables de flux qui seront utilisées dans le trigger **Directory Scanner** et le **connector Salesforce**.

Par exemple :
  * `F.FileName` de type **Alpha**, **Length** = 2000 : variable avec le chemin absolu et le nom du fichier.
  * `F.FileContent` de type **Blob** : variable qui correspond au contenu du fichier.
  * `F.ResultBlob` de type **Blob** : variable qui correspond au contenu stocké.
  * `F.OperationSuccess` de type **Logical** (booléen) : variable qui correspond au résultat de l'opération effectuée.

---

## 5. Scanner le répertoire

[Pour scanner un répertoire -> ./tutos/directory-scanner-trigger.md](./tutos/directory-scanner-trigger.md "Pour scanner un répertoire")

---

## 6. Créer une ressource de connexion automatique à Salesforce

1. Sélectionner **Settings** sous le menu **Project**. La fenêtre **Settings** apparaît.
2. Cliquer sur **Resources**.
3. Cliquer sur **Add**. Une fenêtre **New Resource** apparaît.
4. Dans **Resource Type**, sélectionner **Salesforce**.
5. Dans **Resource Name**, donner un nom à votre ressource.
6. Cliquer sur **OK**. La fenêtre **New Resource** se ferme.
7. Champ Name **Endpoint URL**, ajouter l'URL de connexion sour le champ **Value**.
8. Cliquer sur **OAuth2**. La fenêtre **OAuth2** apparaît.

> Un ou plusieurs messages d'erreur de script peuvent s'afficher. Vous pouvez les ignorer.

Indiquer le nom de connexion dans **Username**.
Indiquer le mot de passe dans **Password**.
Cliquer sur **Log In**.

> **************************** A compléter ****************************

Si l’authentification est bonne. Cliquer sur **Validate**.

9. Cliquer sur **Apply**.
10. Cliquer sur **OK**. La fenêtre **Settings** se ferme.

---

## 7. Ajouter les données d'un fichier CSV vers une base de données Salesforce

1. Déplacer **Salesforce** dans **Toolbox/Connectors** vers le centre de la fenêtre du flux. Il apparaît avec une roue dentée rouge.

> Vérifier dans **Properties/Setting/Resource Name** que la ressource Salesforce choisie apparaît correctement.

2. Clic droit sur ce connecteur **Salesforce** ajouté en 1. Sélectionner **Configuration**. Une fenêtre **Salesforce Configuration** apparaît.
    * Champ **Object** : sélectionner la table où vous souhaitez ajouter des données.
    * Champ **Operation** : sélectionner **Create**.
    * Champ **New Object ID** : sélectionner la variable de flux qui a été utilisée dans le trigger **Directory Scanner**. C'est le chemin absolu avec le nom du fichier. Ici : `F.FileName`.
    *  Champ **Store Result In** : sélectionner **Variable**. Cliquer sur l'icône représentant **…** (3 petits points). Une fenêtre **Variable List** apparaît. Sélectionner la variable de flux `F.ResultBlob` créée préalablement.
    * Champ **Operation Success** : sélectionner **Variable**. Cliquer sur l'icône représentant **…** (3 petits points). Une fenêtre **Variable List** apparaît. Sélectionner la variable de flux `F.OperationSuccess` créée préalablement.

Cliquer sur **OK**. La fenêtre **Salesforce Configuration** se ferme.

3. Un nouvel onglet apparaît avec une fenêtre **Source** et une fenêtre **Destination**.

> La fenêtre **Destination** a déjà le schéma de la base de données de Salesforce.

4. Déplacer **Flat File** de **Toolbox/Mapper Schemas** vers la fenêtre **Source**. Dans **Properties** :
    * Champ **Source Type** : sélectionner **Variable**.
    * Champ **Data Source Encoding** : sélectionner l'encodage correspondant.
    * Champ **Variable** : sélectionner `F.FileContent`.
    * Champ **Include Delimiter In Data** : sélectionner **Yes** si les champs de données sont entre guillements **« »**.
    * Champ **Lines** sur **(Collection)** : cliquer sur l'icône **…** (3 petits points). Une fenêtre **Flat File** apparaît.
      * Soit remplir les champs un à un, soit présenter un fichier d'exemple avec uniquement les en-têtes qui correspondra au champ du fichier à scanner.
      * Compléter les options selon les besoins. Cliquer sur **OK**. La fenêtre **Flat File** se ferme.
    * Champ **Name** : donner un nom à votre source.

> Pour vérifier l'encodage d'un fichier, vous pouvez ouvrir le fichier avec NotePad++ et cliquer sur le menu **Encoding** ou **Encodage**.

5. Connecter le champ **Source** avec le champ **Destination** selon la correspondance souhaitée. Soit d'un clic gauche du champ **Source** jusqu'au champ **Destination**, soit clic droit sur le champ **Source** en sélectionnant **Conect** jusqu'au champ **Destination** correspondant.

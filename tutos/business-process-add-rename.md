[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# 'business process' : ajouter et renommer

## Ajouter un 'business process'

1. Dans **Solution Explorer**, cliquer sur le nom du projet (juste en-dessous de **Solution...** avec l’icône Magic violet).
2. Clic droit et sélectionner **Add/Business Process**.

---

## Renommer un 'business Process'

1. Dans **Solution Explorer**, cliquer sur le **Business Process** désiré.
2. Clic droit et sélectionner **Rename**. Donner un nom et appuyer sur la touche **ENTREE**.

[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# 'flow' : Ajouter et renommer un flux et une description

## Ajouter un flux

Dans **Solution Explorer**, se positionner sur le business process choisi. Clic droit. Sélectionner **Add flow**.

---

## Renommer un flux

1. Dans **Solution Explorer**, se positionner sur le flux choisi. Clic droit. Sélectionner **Rename**.
2. Ecrire un nom et appuyer sur la touche **ENTREE**.

---

## Ajouter une description pour un flux

Cliquer sur le flux. Aller dans **Properties/Description** et ajouter une description.

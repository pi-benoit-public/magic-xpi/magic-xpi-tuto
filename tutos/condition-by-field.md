[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# Condition selon un champ

Imaginons que vous ayez une colonne `id` et une colonne `date`.
Vous souhaitez faire une modification de la date seulement sur un id particulier.

1. Créer une variable de flux nommée `F.Id` de type **alpha** et de **Length : 3** (le type et *length* peuvent varier selon vos données sources).

2. Dans **Data Mapper/Destination**, clic droit sur le 1er niveau (1er dossier).
Sélectionner **Expand all**.

3. Cliquer sur **Record** ou **Row** ..., selon.

4. Dans **Properties/General/Multi Updates**, cliquer sur l’icône **...** (3 petits points).

5. Une fenêtre **Multi Updates** apparaît. Cliquer sur **Add**.
    * Champ **Variable Name** : ajouter votre variable de flux `F.Id`.
    * Champ **Expression** :
      * Cliquer sur l'icône en forme de crayon. Une fenêtre **Src.S2/Record : Record - Expression [Alpha]** apparaît (si la destination est un **Flat File** sinon **Record** sera remplacé par **Row** *à confirmer...*).
      * Sélectionner dans **Source Nodes** (menu déroulant avec comme icône **</>**), la variable correspondant au champ d’en-tête `id` : **Src.S1/Record/id**.
      * Cliquer sur **OK**. La fenêtre disparaît.

Cliquer sur le point d’exclamation blanc sur fond rouge. Si tout est correct. Il disparaît.

Cliquer sur **OK**. La fenêtre disparaît.

6. Toujours sous la fenêtre **Destination**, cliquer sur `date`.

7. Dans les **Properties/Calculate Value** de `date`, cliquer sur l’icône **...** (3 petits points). Une fenêtre **Destination/Record/Date : Calculate Value [Alpha]** apparaît.

8. Ajouter votre condition, par exemple : `IF ( F.Id = '2', '', 'Src.S1/Record/date' )`.

9. Cliquer sur **Verify**. Si votre expression est valide. Cliquer sur **OK**. La fenêtre disparaît. Sinon, corrigez votre expression.

10. **fx** apparaît dans l'icône en forme de flèche pointant sur `date`.

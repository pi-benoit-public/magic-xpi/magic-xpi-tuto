[<- Sommaire](../README.md "Sommaire")

| Logiciel  | Version |
|-----------|---------|
| Magic xpi |     4.7 |

# 'Trigger' - 'Directory Scanner' : Pour scanner un répertoire

1. Dans **Solution Explorer**, double cliquer sur le flux choisi. Une fenêtre apparaît avec un bandeau indiqué **Triggers (0)** en haut à droite.

2. Sélectionner **Directory Scanner** dans **Toolbox/Triggers**. Le déplacer dans **Triggers** du flux en appuyant sur la touch **Ctrl**.

> En cas de difficulté pour mettre le **Directory Scanner** dans **Triggers** du flux, cliquer sur la flèche en haut à gauche du bandeau blanc pour l’agrandir.

3. Lorsque le **Directory Scanner** est dans **Triggers** du flux, il apparaît avec une roue dentée rouge. Ceci signifie qu’il n’est pas encore configuré.
Pour le configurer, double cliquer dessus ou clic droit et sélectionner **Configuration**.

4. Une fenêtre nommée **Component Configuration: Directory Scanner** apparaît.
Cliquer sur **New**.
    * Champ **Source** : sélectionner **LAN** pour scanner un répertoire local.
    * Champ **Directory** : cliquer sur l’icône crayon. Une fenêtre nommée **Directory Scanner: Operation Details – Directory [Alpha]** apparaît. Mettre le chemin du répertoire **IN**.
    * Champ **Filter** : `*.*` où `*` correspond à n’importe quelle chaîne de caractères.
    * Champ **Action** : sélectionner **Move** pour déplacer le fichier du répertoire **IN** vers le répertoire **OUT**.
    * Champ **Destination Directory** : même action que le champ **Directory** avec une destination avec un répertoire **OUT**.

Laisser ou compléter les autres champs selon les spécifications de votre flux.

5. Cliquer sur **Advanced**. Une fenêtre nommée **Log and Return Details** apparaît.
    * Champ **Return file to** : sélectionner une variable de flux de type **BLOB** que vous avez préalablement créé (cf. : [créer des variables de flux](./tutos/flow-add-variable.md "créer des variables de flux")). Par exemple : `F.FileContent`.
    * Champ **Return destination file name to** : ce champ peut être vide.
    * Champ **Return source file name to** : sélectionner une variable de flux de type **ALPHA** que vous avez préalablement créé. Par exemple : `F.FileName`.
    * Champ **Select a location for the arguments XML** : ce champ peut être vide.

Cliquer sur **OK**. La fenêtre **Log and Return Details** se ferme.

Cliquer sur **OK**. La fenêtre **Component Configuration: Directory Scanner** se ferme.

Le Trigger **Directory Scanner** est configuré.

> Penser à sauvegarder votre travail régulièrement, soit en appuyant sur les touches **Ctrl + S** (enregistre le flux), soit en appuyant sur les touches **Ctrl + shift + S**, soit dans le menu **File/Save...**.
